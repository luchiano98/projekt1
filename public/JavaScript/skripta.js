var config = {
            apiKey: "AIzaSyAjve5gSmpOxrCwrlNo-2iDluDZk7gmYxA",
            authDomain: "db-socsport.firebaseapp.com",
            databaseURL: "https://db-socsport.firebaseio.com",
            projectId: "db-socsport",
            storageBucket: "db-socsport.appspot.com",
            messagingSenderId: "670565779053"
            };
            firebase.initializeApp(config);
            const db = firebase.firestore();
            db.settings({ timestampsInSnapshots: true }); 
var sessionStorage = window.sessionStorage;

//funkcija, ki se izvrši ob kliku na login button(ko se loginamo)
function loginFunc() {
    var email = document.getElementById("mailLogin").value;
    var password = document.getElementById("passwLogin").value;
    db.collection("users").where("mail", "==", email)
    .get()
    .then(function(querySnapshot) {
        //preverimo, ali smo našli sploh kakšen dokument z vpisanim mailom
        if(!querySnapshot.empty) {
            querySnapshot.forEach(function(doc) {
                // preverimo ali je geslo, s katerim se je uporabnik registriral enako vpisanemu
                if(doc.data().password == password) {
                    document.getElementById("passwLogin").value = "";
                    document.getElementById("mailLogin").value = "";
                    document.getElementById("responseLogin").innerHTML = "Redirecting...";
                    window.location.href = "/Homepage.html";
                } else {
                    document.getElementById("passwLogin").value = "";
                    document.getElementById("mailLogin").value = "";
                    document.getElementById("responseLogin").innerHTML = "You entered wrong password, please try again.";
                }
            });
        } else {
            //v primeru, da ne najdemo dokumenta z vpisanim mailom
            document.getElementById("passwLogin").value = "";
            document.getElementById("mailLogin").value = "";
            document.getElementById("responseLogin").innerHTML = "There is no profile with such e-mail.";
        }
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
    sessionStorage.setItem("email", email);
}

//funkcija, ki se izvrši ob kliku na register button
function registerFunc() {
    var first = document.getElementById("firstNameRegister");
    var last = document.getElementById("lastNameRegister");
    var email = document.getElementById("mailRegister");
    var passw = document.getElementById("passwRegister");
    var confirm = document.getElementById("passwRegisterConfirm");
    var response = document.getElementById("responseRegister");
    //preverimo, ali je kakšno polje neizpolnjeno
    if((first.value != null && first.value != "") && (last.value != null && last.value != "") && (email.value != null && email.value != "") && 
    (passw.value != null && passw.value != "") && (confirm.value != null && confirm.value != "")) {
        //preverimo ali sta vpisana gesla enaka
        if(passw.value == confirm.value) {
            db.collection("users").add({
               name: first.value,
               surname: last.value,
               mail: email.value,
               password: passw.value
            }).then(function(docRef) {
                response.innerHTML = "Thank you for registering";
                window.location.href = "/";
            })
            //napaka pri shranjevanju v podatkovno bazo
            .catch(function(error) {
                response.innerHTML = "It seems there was an error while saving data to database, please try again";
                first.value = "";
                last.value = "";
                email.value = "";
                passw.value = "";
                confirm.value = "";
            }); 
        }
        //v primeru, da se gesla ne ujemata
        else {
            passw.value = "";
            confirm.value = "";
            response.innerHTML = "Passwords do not match."
        }
    } else {
        passw.value = "";
        confirm.value = "";
        response.innerHTML = "Please fill in required fields."
    }
}

//inicializacija mape ob kreiranju novega eventa
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var markers = [];
//API key = AIzaSyCOfjETD_sgTff-FKe3DPLi-SzVPyygeM4
function initMap(mapDivName, eventLat, eventLong) {
    // Create a map object and specify the DOM element
    // for display.
    console.log(mapDivName);
    directionsDisplay = new google.maps.DirectionsRenderer();
    var myMap = document.getElementById(mapDivName);
    if (navigator.geolocation) {
        var coords = navigator.geolocation.getCurrentPosition(function(position) {
            var options = {
                center: {lat:position.coords.latitude,lng:position.coords.longitude},
                zoom: 12
            };
            
            var pyrmont = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
            map = new google.maps.Map(document.getElementById(mapDivName), options);
            //marker na naši lokaciji
            var markerMe = new google.maps.Marker({
                map: map,
                position: {lat:position.coords.latitude,lng:position.coords.longitude}
            }); 
            directionsDisplay.setMap(map);
            var infowindowMe = new google.maps.InfoWindow();
            google.maps.event.addListener(markerMe, 'click', function() {
                infowindowMe.setContent("Your location");
                infowindowMe.open(map, this);
            });
            //ob kliku na mapo se nam prikaže marker, kjer naj bi bilo igrišče, če smo na strani za kreiranje eventa;
            if(document.title=="CreateEvent") {
                google.maps.event.addListener(map, 'click', function(e) {
                    //vedno želimo imeti zgolj en marker na mapi, zato v primeru, da je na mapi že kakšen marker tega zbrišemo ob kliku na mapo(inicializacija novega markerja)
                    if(markers.length != 0) {
                        markers[0].setMap(null);
                    }
                    var latLng = e.latLng;
                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: map
                    });
                    markers[0] = marker
                    console.log(JSON.stringify(latLng));
                    document.getElementById("crtEvLat").value = latLng.lat();
                    document.getElementById("crtEvLng").value = latLng.lng();
                    map.panTo(latLng);
                    //moja geolokacija
                    var start = markerMe.position;
                    //geolokacija igrišča
                    var end = latLng;
                    
                     var request = {
                        origin: start,
                        destination: end,
                        travelMode: google.maps.TravelMode.DRIVING
                    };
                    //nastavitev poti
                    directionsService.route(request, function(result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            directionsDisplay.setDirections(result);
                        } else {
                            alert("Couldn't get directions:" + status);
                        }
                    });
                }); 
            }
            if(document.title=="Projekt1") {
                 var markerEvent = new google.maps.Marker({
                    map: map,
                    position: {lat: parseFloat(eventLat),lng: parseFloat(eventLong)}
                }); 
                map.panTo(markerEvent.position);
                //moja geolokacija
                var start = markerMe.position;
                //geolokacija igrišča
                var end = markerEvent.position;
                
                 var request = {
                    origin: start,
                    destination: end,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                //nastavitev poti
                directionsService.route(request, function(result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(result);
                    } else {
                        alert("Couldn't get directions:" + status);
                    }
                });
            }
        });
    } else {
        myMap.innerHTML = "Geolocation is not supported by this browser.";
    }
}

//funkcija, ki se izvede ob kliku na gumb "Create Event"(ustvari nov dogodek v podatkovni bazi)
function createEventFunc() {
    //pridobivanje vrednosti iz obrazca
    var date = document.getElementById("crtEvDate").value;
    var time = document.getElementById("crtEvTime").value;
    var price =document.getElementById("crtEvPrice").value;
    var maxPlayers = document.getElementById("crtEvMaxPlayers").value;
    var minPlayers = document.getElementById("crtEvMinPlayers").value;
    var lat = document.getElementById("crtEvLat").value;
    var lng = document.getElementById("crtEvLng").value;
    var sport = document.querySelector('input[name="sport"]:checked', "#sportForm").value;
    console.log(sport);
    var position = {latitude: lat, longtitude: lng};
   if(date != null && date != "" && time != null && time != "" && price != null && price != "" && maxPlayers != null && maxPlayers != "" && minPlayers != null && minPlayers != "" && lat != null && lat != "" && lng != null && lng != "" && sport != null) {
        var nameOfEvent;
        var mailLogiranega = sessionStorage.getItem("email");
        //Poiščemo ime našega uporabnika, znotraj iskanja tudi nastavimo nov dokument, saj je Javascript asinhron in bi lahko naprej izvajal kodo, kar bi pomenilo operiranje z null objekti
        db.collection("users").where("mail", "==", mailLogiranega)
            .get()
            .then(function(querySnapshot) {
                querySnapshot.forEach(function(doc) {
                   nameOfEvent = date +"-" + time + "-"+doc.data().name + "-"+doc.data().surname;
                   //Nastavitev dokumenta, ki vsebuje informacije o eventu(format imena: YYYY-MM-DD-HH:MM-IME-PRIIMEK)
                   db.collection("events").doc(nameOfEvent).set({
                      location: position,
                      mail_all: [mailLogiranega],
                      mail_org: mailLogiranega,
                      players_maximum: maxPlayers,
                      players_minimum: minPlayers,
                      date: date,
                      time:time,
                      price: price,
                      players_ready: 1,
                      sport: sport
                    });
                    document.getElementById("crtEvResponse").innerHTML = "You have succesfully created an event.";
                });
                
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
            });
    } else {
        //v primeru, da niso izpolnjena polja
        document.getElementById("crtEvResponse").innerHTML = "Please fill in required fields.";
    }
}

//funkcija, ki se izvede ob kliku na "Join" gumb => pridružimo se eventu
function joinEventFunc(e) {
    e = e || window.event;
    var data = [];
    var target = e.srcElement || e.target;
    while (target && target.nodeName !== "TR") {
        target = target.parentNode;
    }
    if (target) {
        //dobimo element, na katerega smo kliknili in pogledamo vsa polja
        var cells = target.getElementsByTagName("td");
        var docName ="";
        var coordName = cells[0].innerHTML.split(' ');
        var today = new Date();
        var month = today.getMonth() + 1;
        if(month < 10) {
            month = "0" + JSON.stringify(month);
        }
        var todayDate = today.getFullYear() + "-" + month +"-" + today.getDate();
        //glede na polja poiščemo ime dokumenta v podatkovni bazi
        docName +=todayDate + "-" + cells[5].innerHTML + "-" + coordName[0] + "-" + coordName[1];
        console.log(docName);
        //glede na ime posodobimo število players_ready polja in dodamo v tabelo mail_all dodanega igralca, ob tem še preverimo ali je igralec že dodan!
        db.collection("events").doc(docName)
            .get()
            .then(doc => {
              if (!doc.exists) {
                document.getElementById("todaysEventsResponse").innerHTML = 'There seems to be a problem with our servers, we apologize!';
              } else {
                var vsiMaili = doc.data().mail_all;
                var isReady = false;
                for(var i = 0; i < vsiMaili.length; i++) {
                    if(sessionStorage.getItem("email") == vsiMaili[i]) {
                        isReady = true;
                    }
                }
                if(isReady) {
                    document.getElementById("todaysEventsResponse").innerHTML = 'You are already part of this event.';
                } else {
                    //V mail_all, kjer so shranjeni maili vseh, ki so vključeni v event, moramo dodati naš mail(ko kliknemo na "join event" gumb moramo posodobiti podatkovno bazo)
                    vsiMaili.push(sessionStorage.getItem("email"));
                    db.collection("events").doc(docName).update({
                        players_ready: doc.data().players_ready + 1,
                        mail_all: vsiMaili
                    })
                }
              }
            })
            .catch(err => {
              console.log('Error getting document', err);
            });
    }
}



//funkcionalnosti, ki se dogajajo le na Homepage strani
if(document.title == "Projekt1") {
    $(document).ready(function() {
        document.getElementById("showLocationDiv").style.display = "none";
        dragElement(document.getElementById("showLocationDiv"));
        //inicializacija naše lokacije
        var myLocationLat;
        var myLocationLong;
        var coords = navigator.geolocation.getCurrentPosition(function(position) {
            myLocationLat = position.coords.latitude;
            myLocationLong = position.coords.longitude;
        });
        //ko se naloži strani pozdravimo uporabnika
        var mailLogiranega = sessionStorage.getItem("email");
        db.collection("users").where("mail", "==", mailLogiranega)
            .get()
            .then(function(querySnapshot) {
                querySnapshot.forEach(function(doc) {
                    document.getElementById("accInfo").innerHTML = "Hello "+doc.data().name+" "+doc.data().surname+"!";
                });
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
            });
        //Naložimo tudi vse evente, ki se dogajajo danes v okolici 50km od trenutne lokacije
        var today = new Date();
        var month = today.getMonth() + 1;
        if(month < 10) {
            month = "0" + JSON.stringify(month);
        }
        var todayDate = today.getFullYear() + "-" + month +"-" + today.getDate();
        console.log(todayDate);
        db.collection("events").where("date","==",todayDate)
            .get()
            .then(function(querySnapshot) {
                querySnapshot.forEach(function(doc) {
                    //za lažje pisanje kode uporabljam obj namesto doc.data()
                    var obj = doc.data();
                    var mailOrg = obj.mail_org;
                    var loc = obj.location;
                    var time = obj.time;
                    var max = obj.players_maximum;
                    var min = obj.players_minimum;
                    var rdy = obj.players_ready;
                    var price = obj.price;
                    var sport  = obj.sport;
                    var hoursNow = today.getHours();
                    if(hoursNow < 10) {
                        hoursNow = "0" + JSON.stringify(hoursNow);
                    }
                    var minutesNow = today.getMinutes();
                    if(minutesNow < 10) {
                        minutesNow = "0" + JSON.stringify(minutesNow);
                    }
                    var timeEvent = time.split(":");
                    //Pogoj, da prikaže zgolj dogodke danes, ki se še niso zgodili in ki so do 50km oddaljeni od moje lokacije
                    if((hoursNow < Number(timeEvent[0]) || (hoursNow == Number(timeEvent[0]) && minutesNow < Number(timeEvent[1])))  && distance(loc.latitude,loc.longtitude, myLocationLat, myLocationLong, 'K') < 50) {
                     db.collection("users").where("mail", "==", mailOrg)
                        .get()
                        .then(function(querySnapshot) {
                            querySnapshot.forEach(function(docIn) {
                                document.getElementById("todaysEvents").innerHTML +=
                                 "<tr>"
                                +   "<td>"+ docIn.data().name + " " + docIn.data().surname +"</td>"
                                +   "<td>"+ min +"</td>"
                                +   "<td>"+ max +"</td>"
                                +   "<td>"+ rdy +"</td>"
                                +   "<td>"+ price +"</td>"
                                +   "<td>" + time + "</td>"
                                +   "<td><button onClick='showLocation()'> Click to see location</button></td>"
                                +   "<td>" + sport + "</td>"
                                +   "<td><button onClick='joinEventFunc()'>Join event</button></td>"
                                +"</tr>";  
                            });
                        })
                        .catch(function(error) {
                            console.log("Error getting documents: ", error);
                        });   
                    } 
                    
                });
            });
    });
}

//----------------------funkcije, ki naredijo premični element(draggable div)---------------------------------//
function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "Header")) {
    /* if present, the header is where you move the DIV from:*/
    document.getElementById(elmnt.id + "Header").onmousedown = dragMouseDown;
  } else {
    /* otherwise, move the DIV from anywhere inside the DIV:*/
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}
//-----------------------------------KONEC TEH FUNKCIJ-----------------------------------//


//funkcija, ki pokaže showLocationDiv in primerno lokacijo eventa na Google Maps
function showLocation(e) {
    document.getElementById("showLocationDiv").style.display = "inline";
    e = e || window.event;
                var data = [];
                var target = e.srcElement || e.target;
                while (target && target.nodeName !== "TR") {
                    target = target.parentNode;
                }
                if (target) {
                    //dobimo element, na katerega smo kliknili in pogledamo vsa polja
                    var cells = target.getElementsByTagName("td");
                    var docName ="";
                    var coordName = cells[0].innerHTML.split(' ');
                    var today = new Date();
                    var month = today.getMonth() + 1;
                    if(month < 10) {
                        month = "0" + JSON.stringify(month);
                    }
                    var todayDate = today.getFullYear() + "-" + month +"-" + today.getDate();
                    //glede na polja poiščemo ime dokumenta v podatkovni bazi
                    docName +=todayDate + "-" + cells[5].innerHTML + "-" + coordName[0] + "-" + coordName[1];
                    //glede na ime posodobimo število players_ready polja in dodamo v tabelo mail_all dodanega igralca, ob tem še preverimo ali je igralec že dodan!
                    db.collection("events").doc(docName)
                        .get()
                        .then(doc => {
                          if (!doc.exists) {
                            document.getElementById("todaysEventsResponse").innerHTML = 'There seems to be a problem with our servers, we apologize!';
                          } else {
                            initMap("clickMap", doc.data().location.latitude, doc.data().location.longtitude); 
                          }
                        })
                        .catch(err => {
                          console.log('Error getting document', err);
                        });
                }
}
//funkcija, ki skrije showLocationDiv 
function hideLocation() {
    document.getElementById("showLocationDiv").style.display = "none";
}

//funkcija, ki izračuna razdaljo med dvema geolokacijama
function distance(lat1, lon1, lat2, lon2, unit) {
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var radlon1 = Math.PI * lon1/180
        var radlon2 = Math.PI * lon2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return dist
}

if(document.title == "CreateEvent") {
    initMap("map");
    $(document).ready(function() {
        var today = new Date();
        var month = today.getMonth() + 1;
        if(month < 10) {
            month = "0" + JSON.stringify(month);
        }
        var todayDate = today.getFullYear() + "-" + month +"-" + today.getDate();
        document.getElementById("crtEvDate").setAttribute("min", todayDate);
    });
    
}

/*global $, google, firebase*/


//iskanje uporabnikov po dokumentih(če so dokumenti poimenovani po mailih lahko iščemo po mejlih...)
    /*var user = db.collection("users").doc("3aPnRrs4shWgSr6D1SbG");
    var getUser = user.get().then(doc => {
          if (!doc.exists) {
            console.log('No such document!');
          } else {
            console.log('Document data:', JSON.stringify(doc.data()));
          }
        })
        .catch(err => {
          console.log('Error getting document', err);
        });
    
    Inicializacija novega elementa
    db.collection("users").doc(imeDokumenta).set({
      name: ime,
      surname: priimek,
      mail: mail,
      password: passw
    });
    */
    
//branje dokumentov v določeni kolekciji, kjer izpiše le dokumente z določeno lastnostjo!
/*db.collection("users").where("mail", "==", mailLogiranega)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            document.getElementById("TodaysEvents").innerHTML += doc.id + " => " + JSON.stringify(doc.data()) + '\n';
        });
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });*/